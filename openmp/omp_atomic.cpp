#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <my_timer.h>
#include <my_omp.h>

void help(const char *prg, FILE *fp = stderr)
{
   fprintf(fp,"%s: --help|-h --length|-n --segments|-s\n");

   return;
}

int main(int argc, char* argv[])
{
   int nSamples = 1000; // Length of the array ... # of samples.
   int nSegments = 10; // # of entries in the histogram.
   bool use_atomic = true;

   for (int i = 1; i < argc; ++i)
   {
#define check_index(i,str) \
   if ((i) >= argc) \
      { fprintf(stderr,"Missing 2nd argument for %s\n", str); return 1; }

      if (strcmp(argv[i],"--samples") == 0 || strcmp(argv[i],"-n") == 0)
      {
         check_index(i+1,"--samples|-n");
         i++;
         if (isdigit(*argv[i]))
            nSamples = atoi( argv[i] );
      }
      else if (strcmp(argv[i],"--segments") == 0 || strcmp(argv[i],"-s") == 0)
      {
         check_index(i+1,"--segments|-s");
         i++;
         if (isdigit(*argv[i]))
            nSegments = atoi( argv[i] );
      }
      else if (strcmp(argv[i],"--atomic") == 0 || strcmp(argv[i],"-a") == 0)
      {
         use_atomic = true;
      }
      else if (strcmp(argv[i],"--critical") == 0 || strcmp(argv[i],"-c") == 0)
      {
         use_atomic = false;
      }
      else
      {
         help(argv[0], stderr);
         return 0;
      }
   }

   int max_threads = 1;
#ifdef _OPENMP
   max_threads = omp_get_max_threads();
#endif

   printf("OpenMP Parallel atomic example: nSamples= %d, nSegments= %d, num_threads= %d\n", nSamples, nSegments, max_threads);

   float xmin = 0, xmax = 1;
   float *x = new float [nSamples];
   int *count = new int [nSegments];

   srand(100);
   for (int i = 0; i < nSamples; ++i)
   {
      float r = float( rand() ) / RAND_MAX;
      x[i] = xmin + r * (xmax - xmin);
   }

   for (int i = 0; i < nSegments; ++i)
      count[i] = 0;

   myTimer_t t0 = getTimeStamp();

   #pragma omp parallel for default(shared)
   for (int i = 0; i < nSamples; ++i)
   {
      float segment_width = (xmax - xmin) / nSegments;
      int segment_index = floorf( (x[i] - xmin) / segment_width );

      if (use_atomic)
      {
         #pragma omp atomic update
         count[ segment_index ] ++;
      }
      else
      {
         #pragma omp critical
         count[ segment_index ] ++;
      }
      //printf("%f %d\n", x[i], segment_index);
   }

   myTimer_t t1 = getTimeStamp();
   printf("histogram took %f (ms) %d\n", 1000*getElapsedTime(t0,t1), use_atomic);

   for (int i = 0; i < nSegments; ++i)
      printf("count[%d]= %d\n", i, count[i]);

   return 0;
}
