#!/bin/sh

make

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=1
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns1Threads.txt
rm scalabilityResults/output.txt

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=2
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns2Threads.txt
rm scalabilityResults/output.txt


for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=4
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns4Threads.txt
rm scalabilityResults/output.txt

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=8
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns8Threads.txt
rm scalabilityResults/output.txt

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=16
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns16Threads.txt
rm scalabilityResults/output.txt

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=32
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns32Threads.txt
rm scalabilityResults/output.txt

for i in 1000 2000 4000 8000
do
	
	export OMP_NUM_THREADS=64
	echo $i particles >> scalabilityResults/output.txt
	./nbody3 -n $i >> scalabilityResults/output.txt
	 
done
sed '/^N/ d' scalabilityResults/output.txt >  scalabilityResults/parallelRuns64Threads.txt
rm scalabilityResults/output.txt
make clean

cd scalabilityResults/
./cleanup.sh
