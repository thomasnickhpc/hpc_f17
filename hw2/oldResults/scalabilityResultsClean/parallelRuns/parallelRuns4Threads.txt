1000 particles
Average time = 46.886945 (ms) per step with 1000 elements 54.69 KB over 100 steps 46.861377 0.021461 0.003879
2000 particles
Average time = 187.412789 (ms) per step with 2000 elements 109.38 KB over 100 steps 187.365411 0.040392 0.006737
4000 particles
Average time = 748.940367 (ms) per step with 4000 elements 218.75 KB over 100 steps 748.846245 0.081761 0.011973
8000 particles
Average time = 2994.359628 (ms) per step with 8000 elements 437.50 KB over 100 steps 2994.033202 0.303146 0.022834
1000 particles
Average time = 23.778079 (ms) per step with 1000 elements 54.69 KB over 100 steps 23.441274 0.304413 0.032158
2000 particles
Average time = 94.107372 (ms) per step with 2000 elements 109.38 KB over 100 steps 93.674624 0.376323 0.056185
4000 particles
Average time = 374.856599 (ms) per step with 4000 elements 218.75 KB over 100 steps 374.206028 0.534111 0.116192
8000 particles
Average time = 1498.514253 (ms) per step with 8000 elements 437.50 KB over 100 steps 1497.610842 0.683104 0.219621
1000 particles
Average time = 12.545617 (ms) per step with 1000 elements 54.69 KB over 100 steps 11.728296 0.756622 0.060467
2000 particles
Average time = 48.106779 (ms) per step with 2000 elements 109.38 KB over 100 steps 46.861087 1.101290 0.144171
4000 particles
Average time = 188.598122 (ms) per step with 4000 elements 218.75 KB over 100 steps 186.792463 1.570346 0.235033
8000 particles
Average time = 750.397714 (ms) per step with 8000 elements 437.50 KB over 100 steps 748.801145 0.850262 0.745916
