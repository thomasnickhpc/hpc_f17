1000 particles
Average time = 42.667550 (ms) per step with 1000 elements 54.69 KB over 100 steps 42.648102 0.015830 0.003402
2000 particles
Average time = 173.076331 (ms) per step with 2000 elements 109.38 KB over 100 steps 173.040507 0.029505 0.006074
4000 particles
Average time = 676.675942 (ms) per step with 4000 elements 218.75 KB over 100 steps 676.604812 0.059757 0.010990
8000 particles
Average time = 2703.125355 (ms) per step with 8000 elements 437.50 KB over 100 steps 2702.834293 0.270401 0.020102
1000 particles
Average time = 42.419320 (ms) per step with 1000 elements 54.69 KB over 100 steps 42.399881 0.015579 0.003644
2000 particles
Average time = 170.373498 (ms) per step with 2000 elements 109.38 KB over 100 steps 170.337446 0.029660 0.006154
4000 particles
Average time = 678.855545 (ms) per step with 4000 elements 218.75 KB over 100 steps 678.783364 0.060751 0.011118
8000 particles
Average time = 2689.193306 (ms) per step with 8000 elements 437.50 KB over 100 steps 2688.905146 0.267409 0.020141
1000 particles
Average time = 42.041188 (ms) per step with 1000 elements 54.69 KB over 100 steps 42.021887 0.015731 0.003357
2000 particles
Average time = 168.984103 (ms) per step with 2000 elements 109.38 KB over 100 steps 168.947758 0.029744 0.006372
4000 particles
Average time = 674.083380 (ms) per step with 4000 elements 218.75 KB over 100 steps 674.013634 0.058380 0.011107
8000 particles
Average time = 2703.155661 (ms) per step with 8000 elements 437.50 KB over 100 steps 2702.866790 0.267655 0.020457
