parallelRuns16Threads.txt
::::::::::::::
1000 particles
Average time = 2.814769 (ms) per step with 1000 elements 54.69 KB over 100 steps 2.773291 0.013618 0.027636
2000 particles
Average time = 11.001959 (ms) per step with 2000 elements 109.38 KB over 100 steps 10.957146 0.015642 0.028914
4000 particles
Average time = 43.840515 (ms) per step with 4000 elements 218.75 KB over 100 steps 43.765061 0.019572 0.055659
8000 particles
Average time = 176.673637 (ms) per step with 8000 elements 437.50 KB over 100 steps 176.528797 0.033164 0.111428
::::::::::::::
parallelRuns1Threads.txt
::::::::::::::
1000 particles
Average time = 41.731906 (ms) per step with 1000 elements 54.69 KB over 100 steps 41.707285 0.020671 0.003720
2000 particles
Average time = 168.648135 (ms) per step with 2000 elements 109.38 KB over 100 steps 168.605438 0.036157 0.006327
4000 particles
Average time = 669.684650 (ms) per step with 4000 elements 218.75 KB over 100 steps 669.600237 0.072252 0.011912
8000 particles
Average time = 2683.991599 (ms) per step with 8000 elements 437.50 KB over 100 steps 2683.693052 0.276560 0.021531
::::::::::::::
parallelRuns2Threads.txt
::::::::::::::
1000 particles
Average time = 21.916163 (ms) per step with 1000 elements 54.69 KB over 100 steps 21.885120 0.014373 0.016445
2000 particles
Average time = 87.500465 (ms) per step with 2000 elements 109.38 KB over 100 steps 87.448088 0.025326 0.026826
4000 particles
Average time = 349.687550 (ms) per step with 4000 elements 218.75 KB over 100 steps 349.592708 0.044899 0.049696
8000 particles
Average time = 1399.091767 (ms) per step with 8000 elements 437.50 KB over 100 steps 1398.840786 0.150876 0.099690
::::::::::::::
parallelRuns32Threads.txt
::::::::::::::
1000 particles
Average time = 1.470912 (ms) per step with 1000 elements 54.69 KB over 100 steps 1.437552 0.017896 0.015246
2000 particles
Average time = 5.635633 (ms) per step with 2000 elements 109.38 KB over 100 steps 5.586537 0.019984 0.028895
4000 particles
Average time = 22.140199 (ms) per step with 4000 elements 218.75 KB over 100 steps 22.059524 0.023661 0.056785
8000 particles
Average time = 87.818631 (ms) per step with 8000 elements 437.50 KB over 100 steps 87.671703 0.034035 0.112633
::::::::::::::
parallelRuns4Threads.txt
::::::::::::::
1000 particles
Average time = 10.975117 (ms) per step with 1000 elements 54.69 KB over 100 steps 10.945061 0.010671 0.019166
2000 particles
Average time = 43.772562 (ms) per step with 2000 elements 109.38 KB over 100 steps 43.719251 0.016726 0.036361
4000 particles
Average time = 174.862934 (ms) per step with 4000 elements 218.75 KB over 100 steps 174.781289 0.027365 0.054044
8000 particles
Average time = 699.804361 (ms) per step with 8000 elements 437.50 KB over 100 steps 699.611867 0.083135 0.108969
::::::::::::::
parallelRuns64Threads.txt
::::::::::::::
1000 particles
Average time = 0.837719 (ms) per step with 1000 elements 54.69 KB over 100 steps 0.791384 0.025755 0.020285
2000 particles
Average time = 4.334088 (ms) per step with 2000 elements 109.38 KB over 100 steps 4.265402 0.033704 0.034756
4000 particles
Average time = 18.799992 (ms) per step with 4000 elements 218.75 KB over 100 steps 18.643498 0.087715 0.068435
8000 particles
Average time = 74.614734 (ms) per step with 8000 elements 437.50 KB over 100 steps 74.360500 0.120783 0.133042
::::::::::::::
parallelRuns8Threads.txt
::::::::::::::
1000 particles
Average time = 5.511001 (ms) per step with 1000 elements 54.69 KB over 100 steps 5.483741 0.012298 0.014746
2000 particles
Average time = 21.921763 (ms) per step with 2000 elements 109.38 KB over 100 steps 21.868381 0.013785 0.039371
4000 particles
Average time = 87.554713 (ms) per step with 4000 elements 218.75 KB over 100 steps 87.479575 0.019401 0.055512
8000 particles
Average time = 349.841266 (ms) per step with 8000 elements 437.50 KB over 100 steps 349.594699 0.046399 0.199873
